/* jshint node: true*/
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

var port = process.env.PORT || 3000;

var configDB = require('./config/database');
mongoose.connect(configDB.url);
var todos = require('./routes/todos');

mongoose.connection.on('open', function(){
    console.log('Connected to MongoDB');
});

mongoose.connection.on('connected', function(){
    
    var Todo = require('./models/todo');
    
    Todo.remove(function(){
        console.log('Todos successfully removed');
    });
    
    var todos = [];
    
    for(var i = 0; i < 5; i++){
        var todo = new Todo();
        todo.text = "todo"+(i+1);
        todos.push(todo);
    }
    
    todos.forEach(function(todo){
        todo.save(function(err){
            console.log('Todo created: ' + todo.text);
        });
    });
    
});

mongoose.connection.on('error', function(){
    console.err.bind(console, 'MongoDB Error');
});

app.use(express.static(__dirname + '/public'));
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(methodOverride());
app.use('/api', todos);

app.get('*',function(req, res){
    res.sendFile(__dirname + '/public/index.html');
});

app.listen(port, function(){
    console.log('localhost:'+port);
});