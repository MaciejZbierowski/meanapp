/* jshint node: true */
var express = require('express');
var app = express();
var router = express.Router();
var mongoose = require('mongoose');
var Todo = require('../models/todo');

router.route('/todos')
    .get(function(req, res){
        Todo.find(function(err, todos){
            if(err){
                res.send(err);
            } else {
                res.json(todos);
            }
        });
    })
    .post(function(req, res){
        Todo.create({
            text: req.body.text
        }, function(err, todo){
            if(err){
                res.send(err);
            }
            Todo.find(function(err, todos){
                if(err){
                    res.send(err);
                } else {
                    res.json(todos);
                }
            });
        });
    });

router.route('/todos/:todo_id')
    .delete(function(req, res){
        Todo.remove({
            _id: req.params.todo_id
        }, function(err, todos){
            if(err){
                res.send(err);
            }
            Todo.find(function(err, todos){
                if(err){
                    res.send(err);
                } else {
                    res.json(todos);
                }
            });
        });
    });

module.exports = router;